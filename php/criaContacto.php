<?php
include ("db.php");
$dados = $_POST['contactos'];
$registo = json_decode(stripslashes($dados), true);
// (Insert) curl -X POST --form 'contactos={"nome":"Fulano","telefone":"987654321","email":"fulano@sapo.pt"}' http://localhost/~jgr/ExtJS4FirstLook/Code/Chapter%207/app/php/criaContacto.php
$tabela = 'contacto';
$nfields = 0;
$chaves = "";
$valores = "";
foreach ($registo as $key => $value) {
	$sep = ($nfields == 0) ? '' : ', ';
	if ($key != '' && $value != '') {
		$chaves .= $sep . $key;
		$valores .= $sep . "'" . $value . "'";
		$nfields += 1;
	}
}
$sql = "INSERT into " . $tabela . " (" . $chaves . ") values (" . $valores . " ) ";
$affected = &$mdb2 -> exec($sql);
if (PEAR::isError($affected)) {
	$result["success"] = false;
	$result["errors"]["reason"] = $affected -> getMessage();
	$result["errors"]["query"] = $sql;
	die(json_encode($result));
} else {
	// sacar o ID inserido
	$id = $mdb2 -> lastInsertID($tabela, 'id');
	if (PEAR::isError($id)) {
		die($id -> getMessage());
	}
	$query = "select * from " . $tabela;
	$query .= " where id = " . $id;
	$resQuery = $mdb2 -> query($query);
	if (PEAR::isError($resQuery)) {
		$result["success"] = false;
		$result["errors"]["reason"] = $resQuery -> getMessage();
		$result["errors"]["query"] = $query;
		die(json_encode($result));
	}
	$row = $resQuery -> fetchRow(MDB2_FETCHMODE_ASSOC);
	// SUCESSO!
	// passo todo o resultado para o cliente
	$tabela = array();
	array_push($tabela, $row);
	$result["contactos"] = $tabela;
	$result["success"] = true;
	echo json_encode($result);
}
?>